package com.example.phat.android20170817;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.phat.android20170817.service.WebService;
import com.example.phat.android20170817.utils.UserSharePrefUtils;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPassword extends AppCompatActivity implements View.OnClickListener {

    private Button btnNext;
    private EditText etPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        mapping();
    }

    private void mapping() {
        etPassword= (EditText) findViewById(R.id.etPassword);
        btnNext= (Button) findViewById(R.id.btn_Next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnNext)){
//            Intent iGGMap = new Intent(ActivityPassword.this,MapsActivity.class);
//            startActivity(iGGMap);
            Intent iBackToLogin = new Intent(ActivityPassword.this, SignInActivity.class);

            String pass = String.valueOf(etPassword.getText()).trim();
            if(pass.equals("")){
                Toast.makeText(this, "Fill password please", Toast.LENGTH_SHORT).show();
            }
            else{
                final UserSharePrefUtils userSharePrefUtils = UserSharePrefUtils.getInstance(this);

//                Toast.makeText(this, "Register Succeed!", Toast.LENGTH_LONG).show();

                WebService webService = WebService.getWebService();

                String phone = userSharePrefUtils.getPhoneNumber();
                String email = userSharePrefUtils.getEmail();
                webService.signup(email, pass, phone, email, new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        if (response.isSuccessful()){
                            if(response.body().get("err_code").getAsInt() ==0){
                                String accessToken = response.body().get("access_token").getAsString();
                                userSharePrefUtils.setAccessToken(accessToken);
                                String refreshToken = response.body().get("refresh_token").getAsString();
                                userSharePrefUtils.setRefreshToke(refreshToken);
                            }else{
                                Toast.makeText(ActivityPassword.this, response.body().get("err_msg").getAsString(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        Log.i("LTP", "onFailure: "+t);
                    }
                });

                startActivity(iBackToLogin);
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            }
        }
    }
}
