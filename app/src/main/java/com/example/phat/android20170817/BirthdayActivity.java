package com.example.phat.android20170817;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BirthdayActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_Next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birthday);
        mapping();
    }

    private void mapping() {
        btn_Next= (Button) findViewById(R.id.btn_Next);
        btn_Next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btn_Next)){
            Intent iGender = new Intent(BirthdayActivity.this,GenderActivity.class);
            startActivity(iGender);
        }
    }
}
