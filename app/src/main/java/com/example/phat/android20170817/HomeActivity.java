package com.example.phat.android20170817;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

    }

    public void btnChatClick(View view) {
        Intent iChat = new Intent(HomeActivity.this,ChatActivity.class);
        startActivity(iChat);
    }

    public void btnProfileClick(View view) {
        Intent iProfile = new Intent(HomeActivity.this,ProfileActivity.class);
        startActivity(iProfile);
    }
}
