package com.example.phat.android20170817;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.phat.android20170817.service.WebService;
import com.example.phat.android20170817.utils.UserSharePrefUtils;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private final String BASE_URL = "http://mobisci-lab.com:8090/";
    private ImageButton ibtnAvatar;
    private Button btnOK;
    private File imageFromGallery = null;
    private EditText etFName;
    private EditText etLName;
    private EditText etPhoneNumber;
    private EditText etCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mapping();

        new LoadAvatar().execute();
    }

    private void mapping() {
        etFName = (EditText)findViewById(R.id.etFirstName);
        etLName = (EditText)findViewById(R.id.etLastName);
        etPhoneNumber = (EditText)findViewById(R.id.etPhoneNumber);
        etCity = (EditText)findViewById(R.id.etCity);
        initDefaultEditText();
        ibtnAvatar = (ImageButton)findViewById(R.id.ibtnAvatar);
        btnOK = (Button)findViewById(R.id.btnOK);

        ibtnAvatar.setOnClickListener(this);
        btnOK.setOnClickListener(this);
    }

    private void initDefaultEditText() {
         UserSharePrefUtils userSharePrefUtils=UserSharePrefUtils.getInstance(this);
        etFName.setText(userSharePrefUtils.getFirstname());
        etLName.setText(userSharePrefUtils.getLastname());
        etPhoneNumber.setText(userSharePrefUtils.getPhoneNumber());
        etCity.setText(userSharePrefUtils.getCity());

    }


    @Override
    public void onClick(View v) {
        if(v.equals(ibtnAvatar)){
            Intent iGetImageFromGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(iGetImageFromGallery,1);

        }
        if(v.equals(btnOK)){
            final String fname = etFName.getText().toString();
            final String lname = etLName.getText().toString();
            final String phNumer = etPhoneNumber.getText().toString();
            final String city = etCity.getText().toString();
            final UserSharePrefUtils userSharePrefUtils=UserSharePrefUtils.getInstance(this);

            WebService webService = WebService.getWebService();

            webService.updateProfile(this, userSharePrefUtils.getAccessToken()
                    , fname, lname, phNumer, city, new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            if(response.isSuccessful()){
                                Log.i("LTP", "onResponse: "+response.body());
                            }else{
                                Toast.makeText(ProfileActivity.this, response.body().get("err_msg").getAsString(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            Log.i("LTP", "onFailure: "+t);
                        }
                    }
            );


            if(imageFromGallery!=null){
                Log.i("LTP", "onClick: path: "+imageFromGallery.getPath()+" \nName: "+imageFromGallery.getName());
                webService.addProfileResource(userSharePrefUtils.getAccessToken(), imageFromGallery, new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            if(response.body().get("err_code").getAsInt()==0){
                                userSharePrefUtils.setFirstname(fname);
                                userSharePrefUtils.setLastname(lname);
                                userSharePrefUtils.setPhoneNumber(phNumer);
                                userSharePrefUtils.setCity(city);
                            }
                            Log.i("LTP", "onResponse: "+response.body());
                        }else{
//                            Toast.makeText(ProfileActivity.this, response.body().get("err_msg").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        Log.i("LTP", "onFailure: "+t);
                    }
                });
            }



            finish();
        }
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:{
                if(resultCode==RESULT_OK){
                    Uri selectedImage = data.getData();
                    Bitmap bitmap;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),bitmap);
                        ibtnAvatar.setBackground(bitmapDrawable);

                        imageFromGallery = new File(getPath(selectedImage));
                        Log.i("LTP", "onClick: path: "+imageFromGallery.getPath()+" \nName: "+imageFromGallery.getName());
                    } catch (IOException e) {
                        e.printStackTrace();
                        imageFromGallery=null;
                        Toast.makeText(this, "File not found", Toast.LENGTH_SHORT).show();
                    }
//                    imageFromGallery = new File(selectedImage.getPath());
                }
            }
        }
    }

    private class LoadAvatar extends AsyncTask<String, Bitmap, Void>{
        @Override
        protected Void doInBackground(String... params) {
            RequestCreator img = Picasso.with(getApplicationContext())
                    .load(BASE_URL + UserSharePrefUtils.getInstance(getApplicationContext()).getAvatarLink())
                    .placeholder(R.drawable.ic_profile_avatar)
                    .error(R.drawable.ic_profile_avatar);

            Bitmap bitmap=null;
            try {
                bitmap = img.get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            publishProgress(bitmap);
            return null;
        }


        @Override
        protected void onProgressUpdate(Bitmap... values) {
            super.onProgressUpdate(values);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),values[0]);
            ibtnAvatar.setBackground(bitmapDrawable);
        }
    }
}
