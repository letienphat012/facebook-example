package com.example.phat.android20170817;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MobileNumberActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etPhoneNumber;
    private Button btnNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);
        mapping();
    }

    private void mapping() {
        etPhoneNumber= (EditText) findViewById(R.id.etPhoneNumber);
        btnNext= (Button) findViewById(R.id.btn_Next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnNext)){
            SharedPreferences accountPhone = getSharedPreferences("UserInfo",0);

            SharedPreferences.Editor phoneEditor = accountPhone.edit();
            String phoneNum = String.valueOf(etPhoneNumber.getText());
            if(phoneNum.equals("")){
                Toast.makeText(this, "Look like you have not written anything about your phone yet, do it first", Toast.LENGTH_SHORT).show();
            }
            else{
                phoneEditor.putString("phoneNumber", phoneNum);
                phoneEditor.commit();
                Intent iPassword = new Intent(MobileNumberActivity.this,ActivityPassword.class);
                startActivity(iPassword);
            }

        }
    }
}
