package com.example.phat.android20170817;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mapping();
    }

    private void mapping() {
        btnSignUp= (Button) findViewById(R.id.btnSignUp);

        btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnSignUp)){
            Intent iRegister = new Intent(SignUpActivity.this,EmailActivity.class);
            startActivity(iRegister);
        }
    }
}
