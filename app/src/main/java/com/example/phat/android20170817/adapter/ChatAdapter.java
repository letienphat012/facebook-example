package com.example.phat.android20170817.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.phat.android20170817.R;
import com.example.phat.android20170817.entities.ChatMessage;

import java.util.List;

/**
 * Created by Phat on 2017-10-19.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.VH>{

    List<ChatMessage> chatHistory;

    public ChatAdapter(List<ChatMessage> chatHistory) {
        this.chatHistory = chatHistory;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_bubble,parent,false);
        VH vh = new VH(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.tvMessage.setText(chatHistory.get(position).getMessage());
        holder.tvDate.setText(chatHistory.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return chatHistory.size();
    }

    class VH extends RecyclerView.ViewHolder{
        TextView tvMessage;
        TextView tvDate;
        public VH(View itemView) {
            super(itemView);
            tvMessage= (TextView) itemView.findViewById(R.id.tvMessage);
            tvDate= (TextView) itemView.findViewById(R.id.tvDateTime);
        }


    }
}
