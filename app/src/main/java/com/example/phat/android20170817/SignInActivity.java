package com.example.phat.android20170817;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phat.android20170817.service.WebService;
import com.example.phat.android20170817.utils.UserSharePrefUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnLogin;
    private TextView tv_ForgotPassword,tv_SignUp;
    EditText etPass,etMail;
    private WebService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED
                ||ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED
                ||ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED
                ||ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED
                ||ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
                ){
            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.INTERNET
                    ,Manifest.permission.ACCESS_FINE_LOCATION
                    ,Manifest.permission.ACCESS_COARSE_LOCATION
                    ,Manifest.permission.READ_EXTERNAL_STORAGE
                    ,Manifest.permission.WRITE_EXTERNAL_STORAGE
            },0x0);
        }

        mapping();

    }

    private void mapping() {
        etMail= (EditText) findViewById(R.id.etMail);
        etPass= (EditText) findViewById(R.id.etPass);
        btnLogin= (Button) findViewById(R.id.btnLogin);
        tv_SignUp= (TextView) findViewById(R.id.tv_SignUp);
        tv_SignUp.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(tv_SignUp)){
            Intent iSignUp = new Intent(SignInActivity.this,SignUpActivity.class);
            startActivity(iSignUp);
        }
        if(v.equals(btnLogin)){
//            SharedPreferences info = getSharedPreferences("UserInfo",0);
//            String phone = info.getString("phoneNumber","fail");
//            String pass = info.getString("password","no");


            if(TextUtils.isEmpty(etMail.getText())){
                Toast.makeText(this, "Please enter Email or Phone number", Toast.LENGTH_SHORT).show();
                return;
            }

            if(TextUtils.isEmpty(etPass.getText())){
                Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
                return;
            }

            String inputMail = String.valueOf(etMail.getText());
            String inputPass = String.valueOf(etPass.getText());
            final UserSharePrefUtils userSharePrefUtils=UserSharePrefUtils.getInstance(this);
            userSharePrefUtils.clearAllInfo();
            service = WebService.getWebService();
            service.signin(inputMail, inputPass, new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    if(response.isSuccessful()){
                        JsonObject object = response.body();
                        if(response.body().get("err_code").getAsInt() ==0){

//                            Toast.makeText(SignInActivity.this, "Login success", Toast.LENGTH_SHORT).show();
                            JsonObject data = object.get("data").getAsJsonObject();
                            userSharePrefUtils.setAccessToken(data.get("access_token").getAsString());
                            userSharePrefUtils.setRefreshToke(data.get("refresh_token").getAsString());

                            JsonObject profile = data.get("profile").getAsJsonObject();

                            if(profile!=null){
                                Gson gson = new Gson();
                                JsonObject pf = gson.fromJson(profile, JsonElement.class).getAsJsonObject();
                                userSharePrefUtils.setPhoneNumber(pf.get("mobile").getAsString());
                                userSharePrefUtils.setGender(pf.get("gender").getAsString());
                                userSharePrefUtils.setEmail(pf.get("email").getAsString());
                                userSharePrefUtils.setLastname(pf.get("lastname").getAsString());
                                userSharePrefUtils.setFirstname(pf.get("firstname").getAsString());
                                userSharePrefUtils.setCity(pf.get("city").getAsString());
                                userSharePrefUtils.setAvatarLink(pf.get("avatar_link").getAsString());
                            }

                            userSharePrefUtils.setLoggedIn(true);
                            Intent intentHome = new Intent(SignInActivity.this,HomeActivity.class);
                            startActivity(intentHome);
                            finish();
                        }
                        else{
                            Toast.makeText(SignInActivity.this, object.get("err_msg").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.i("LTP", "onFailure: "+t);
                    Toast.makeText(SignInActivity.this, "Something wrong with your connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void tvTitleClick(View view) {
        Intent iMap = new Intent(SignInActivity.this,MapsActivity.class);
        startActivity(iMap);
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences = getSharedPreferences("AutoLogin",MODE_PRIVATE);
        String at =sharedPreferences.getString("access_token","");
    }
}
