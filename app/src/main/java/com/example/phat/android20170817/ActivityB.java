package com.example.phat.android20170817;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivityB extends AppCompatActivity implements View.OnClickListener {


    TextView tv_FromA;
    Button btn_ToA,btn_ToC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        setTitle("B");
        mapping();
        Intent intent = getIntent();
        tv_FromA.setText("From A: ");
        tv_FromA.append(intent.getStringExtra("textFromA"));

    }

    private void mapping(){
        tv_FromA= (TextView) findViewById(R.id.tv_FromA);
        btn_ToA= (Button) findViewById(R.id.btn_ToA);
        btn_ToC= (Button) findViewById(R.id.btn_ToC);

        btn_ToA.setOnClickListener(this);
        btn_ToC.setOnClickListener(this);
    }

    int bRequestCode = 0x1;
    @Override
    public void onClick(View v) {
        if(v.equals(btn_ToC)){
//            Intent intentC = new Intent(ActivityB.this,ActivityC.class);
//            startActivityForResult(intentC, bRequestCode);
            Intent intent = new Intent();
            intent.putExtra("textToA","To C");
            setResult(0x0,intent);
            finish();
        }
        if(v.equals(btn_ToA)){
            Intent intent = new Intent();
            intent.putExtra("textToA","To B");
            setResult(0x0,intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode== bRequestCode){
            finish();
        }
    }
}
