package com.example.phat.android20170817;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_Start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first20180817);
        setTitle("A");
        mapping();
        Toast.makeText(MainActivity.this,"Welcome to A",Toast.LENGTH_LONG).show();
    }

    private void mapping(){
        btn_Start= (Button) findViewById(R.id.btn_Start);
        btn_Start.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btn_Start)){
            Intent intentB = new Intent(MainActivity.this,ActivityB.class);
            startActivity(intentB);
        }

    }
}
