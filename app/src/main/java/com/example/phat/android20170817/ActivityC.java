package com.example.phat.android20170817;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityC extends AppCompatActivity implements View.OnClickListener {

    Button btn_ToB,btn_ToA;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c);
        setTitle("C");
        mapping();
    }
    private void mapping(){
        btn_ToB= (Button) findViewById(R.id.btn_ToB);
        btn_ToA= (Button) findViewById(R.id.btn_ToA);

        btn_ToA.setOnClickListener(this);
        btn_ToB.setOnClickListener(this);
    }

    int cRequestCode = 0x2;
    @Override
    public void onClick(View v) {
        if(v.equals(btn_ToA)){
            Intent intentA = new Intent(ActivityC.this,ActivityA.class);
            startActivityForResult(intentA,cRequestCode);
            intentA.putExtra("textToA","To C");
        }
        if(v.equals(btn_ToB)){
            Intent intent = new Intent();
            intent.putExtra("textToB","To C");
            setResult(0x1,intent);
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==cRequestCode){
            finish();
        }
    }
}
