package com.example.phat.android20170817;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.phat.android20170817.adapter.ChatAdapter;
import com.example.phat.android20170817.entities.ChatMessage;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChatActivity extends AppCompatActivity {

    private RecyclerView rvMessage;
    private List<ChatMessage> chatHistory;
    private ChatAdapter adapter;
    private EditText etMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        etMessage =(EditText)findViewById(R.id.etMessage);
        chatHistory = new ArrayList<>();
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvMessage = (RecyclerView)findViewById(R.id.rvMessage);
        adapter = new ChatAdapter(chatHistory);

        rvMessage.setLayoutManager(llm);
        rvMessage.setAdapter(adapter);
    }

    public void btnSendClick(View view) {

        String msg = etMessage.getText().toString().trim();
        ChatMessage chatMessage= new ChatMessage(msg, DateFormat.getDateTimeInstance().format(new Date()));
        chatHistory.add(chatMessage);
        adapter.notifyDataSetChanged();

        rvMessage.scrollToPosition(chatHistory.size()-1);
        etMessage.setText("");
    }
}
