package com.example.phat.android20170817;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityA extends AppCompatActivity implements View.OnClickListener {

    EditText et_Msg;
    Button btn_Start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);
        setTitle("A");
        mapping();
    }

    private void mapping(){
        et_Msg= (EditText) findViewById(R.id.et_Msg);
        btn_Start= (Button) findViewById(R.id.btn_Start);
        btn_Start.setOnClickListener(this);
    }

    int aRequestCode = 0x0;
    @Override
    public void onClick(View v) {
        if(v.equals(btn_Start)){
            Intent intentB = new Intent(ActivityA.this,ActivityB.class);
//            startActivity(intentB);
            intentB.putExtra("textFromA",et_Msg.getText().toString());
            startActivityForResult(intentB,aRequestCode);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == aRequestCode){
            et_Msg.setText(data.getStringExtra("textToA").toString());
        }
    }
}
