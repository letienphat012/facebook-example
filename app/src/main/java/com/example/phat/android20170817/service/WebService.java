package com.example.phat.android20170817.service;

import android.content.Context;
import android.util.Log;

import com.example.phat.android20170817.utils.UserSharePrefUtils;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Phat on 2017-10-17.
 */

public class WebService {
    private static final String BASE_URL="http://mobisci-lab.com:8090/api/";
    private Service mService;
    private static WebService mWebservice = null;

    private WebService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(2,TimeUnit.SECONDS)
                .writeTimeout(2,TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        mService = retrofit.create(Service.class);
    }



    public static synchronized WebService getWebService(){
        if(mWebservice==null){
            mWebservice = new WebService();
        }
        return mWebservice;
    }

    public void signup(String username, String password, String mobile, String email, Callback<JsonObject> callback){
        JsonObject json = new JsonObject();
        json.addProperty("username",username);
        json.addProperty("password",password);
        JsonObject profile = new JsonObject();
        profile.addProperty("mobile",mobile);
        profile.addProperty("email",email);
        json.add("profile",profile);

        Log.i("LTP", "signup: "+json);
        mService.signup(json).enqueue(callback);
    }

    public void addProfileResource(String access_token, File image, Callback<JsonObject> callback){

        RequestBody requestAvatar = RequestBody.create(MediaType.parse("multipart/form-data"),image);

        RequestBody accessToken = RequestBody.create(MediaType.parse("multipart/form-data"),access_token);
        MultipartBody.Part avatar = MultipartBody.Part.createFormData("avatar",image.getName(),requestAvatar);
        Log.i("LTP", "addProfileResource: avatar: "+avatar.toString()+" requestAvatar"+requestAvatar);
        mService.addProfileResources(accessToken,avatar).enqueue(callback);
//        mService.addProfileResources(accessToken,requestAvatar).enqueue(callback);
    }

    public void signin(String username, String password,Callback<JsonObject> callback){
        JsonObject json = new JsonObject();
        json.addProperty("username",username);
        json.addProperty("password",password);
        json.addProperty("pushy_id","");
        mService.signin(json).enqueue(callback);
    }

    /**
     * "profile":{
     "firstname":"dsfsfsdfsdf",
     "lastname":"dfgsfgsdfgfsd",
     "public_name":"asdfadsf",
     "birthday":150893,
     "gender":0,
     "city": "Universe",
     "State":"Nevada"
     }
     */
    public void updateProfile(Context context,String accessToken, String firstName, String lastName, String phoneNumber, String city,Callback<JsonObject> callback){
        String at = UserSharePrefUtils.getInstance(context).getAccessToken();
        JsonObject json = new JsonObject();
        JsonObject newProfile = new JsonObject();

        json.addProperty("access_token",at);

        newProfile.addProperty("firstname",firstName);
        newProfile.addProperty("lastname",lastName);
        newProfile.addProperty("public_name","asdfadsf");
        newProfile.addProperty("birthday","150893");
        newProfile.addProperty("gender","0");
        newProfile.addProperty("city",city);
        newProfile.addProperty("State","Nevada");

        json.add("profile",newProfile);

        mService.updateProfile(json).enqueue(callback);


    }

}
