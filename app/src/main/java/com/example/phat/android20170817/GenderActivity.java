package com.example.phat.android20170817;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class GenderActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);
        mapping();
    }

    private void mapping() {
        btnNext= (Button) findViewById(R.id.btn_Next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnNext)){
            Intent iMobile = new Intent(GenderActivity.this,MobileNumberActivity.class);
            startActivity(iMobile);
        }
    }
}
