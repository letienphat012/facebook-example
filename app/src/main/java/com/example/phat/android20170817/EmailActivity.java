package com.example.phat.android20170817;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.phat.android20170817.service.WebService;
import com.example.phat.android20170817.utils.UserSharePrefUtils;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnNext;
    private EditText etEmail;
    private TextInputLayout tilEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        mapping();
    }

    private void mapping() {
        tilEmail = (TextInputLayout)findViewById(R.id.tilEmail);
        etEmail = (EditText)findViewById(R.id.etEmail);
        btnNext= (Button) findViewById(R.id.btn_Next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnNext)){
            String email = etEmail.getText().toString();
            UserSharePrefUtils.getInstance(this).setEmail(email);
            Intent iBirthday = new Intent(EmailActivity.this,NameActivity.class);
            startActivity(iBirthday);
        }
    }
}
