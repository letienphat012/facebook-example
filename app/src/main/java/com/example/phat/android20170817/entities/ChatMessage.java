package com.example.phat.android20170817.entities;

/**
 * Created by Phat on 2017-10-19.
 */

public class ChatMessage {
    private String message;
    private String date;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {

        return message;
    }

    public String getDate() {
        return date;
    }

    public ChatMessage() {

    }

    public ChatMessage(String message, String date) {

        this.message = message;
        this.date = date;
    }
}
