package com.example.phat.android20170817.service;

import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Phat on 2017-10-17.
 */

public interface Service {
    @Headers("Content-Type:application/json")
    @POST("signup")
    Call<JsonObject> signup(@Body JsonObject json);

    @Headers("Content-Type:application/json")
    @POST("signin")
    Call<JsonObject> signin(@Body JsonObject json);


    @Multipart
    @POST("add_profile_resource")
    Call<JsonObject> addProfileResources(@Part("access_token") RequestBody accessToken
            , @Part MultipartBody.Part avatar);

    @Headers("Content-Type:application/json")
    @POST("update_profile")
    Call<JsonObject> updateProfile(@Body JsonObject json);
}
