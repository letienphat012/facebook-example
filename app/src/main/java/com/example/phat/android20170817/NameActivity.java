package com.example.phat.android20170817;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.phat.android20170817.utils.UserSharePrefUtils;

public class NameActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnNext;
    private EditText etLname;
    private EditText etFName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        mapping();
    }

    private void mapping() {
        etFName = (EditText)findViewById(R.id.etFirstName);
        etLname = (EditText)findViewById(R.id.etLastName);
        btnNext= (Button) findViewById(R.id.btn_Next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnNext)){
            String fName = etFName.getText().toString();
            String lName = etLname.getText().toString();
            UserSharePrefUtils sharePrefInfo = UserSharePrefUtils.getInstance(this);
            sharePrefInfo.setFirstname(fName);
            sharePrefInfo.setLastname(lName);
            Intent iBirthday = new Intent(NameActivity.this,BirthdayActivity.class);
            startActivity(iBirthday);
        }
    }
}
