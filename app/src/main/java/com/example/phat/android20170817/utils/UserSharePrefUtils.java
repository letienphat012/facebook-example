package com.example.phat.android20170817.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class UserSharePrefUtils {

    private static final String USER_INFO = "userInfo";
    private static UserSharePrefUtils instance = null;
    private SharedPreferences sharedPreferences;

    private static final String LOGGED_IN = "loggedIn";
    private static final String PHONE_NUMBER="phoneNumber";
    private static final String EMAIL="email";
    private static final String FIRSTNAME="firstName";
    private static final String LASTNAME="lastName";
    private static final String BIRTHDAY="birthday";
    private static final String GENDER="gender";
    private static final String ACCESS_TOKEN="accessToken";
    private static final String REFRESH_TOKE="refreshToken";
    private static final String CITY = "city";
    private static final String AVATAR_LINK = "avatarLink";

    private UserSharePrefUtils(Context context) {
        sharedPreferences =context.getSharedPreferences(USER_INFO,Context.MODE_PRIVATE);
    }

    public static synchronized UserSharePrefUtils getInstance(Context context){
        if(instance == null){
            instance=new UserSharePrefUtils(context);
        }
        return  instance;
    }

    public String getEmail(){
        return sharedPreferences.getString(EMAIL,"");
    }

    public String getPhoneNumber(){
        return sharedPreferences.getString(PHONE_NUMBER,"");
    }
    public String getFirstname(){
        return sharedPreferences.getString(FIRSTNAME,"");
    }
    public String getLastname(){
        return sharedPreferences.getString(LASTNAME,"");
    }
    public String getBirthday(){
        return sharedPreferences.getString(BIRTHDAY,"");
    }
    public String getGender(){
        return sharedPreferences.getString(GENDER,"");
    }
    public String getAccessToken(){
        return sharedPreferences.getString(ACCESS_TOKEN,"");
    }
    public String getRefreshToke(){
        return sharedPreferences.getString(REFRESH_TOKE,"");
    }
    public String getCity(){return sharedPreferences.getString(CITY,"");}
    public String getAvatarLink(){return sharedPreferences.getString(AVATAR_LINK,"");}
    public boolean isLoggedIn(){return sharedPreferences.getBoolean(LOGGED_IN,false);}

    public void setFirstname(String firstname){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FIRSTNAME,firstname);
        editor.apply();
    }
    public void setLastname(String lastname){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LASTNAME,lastname);
        editor.apply();
    }

    /**
     *
     * @param birthday with format dd-mm-yy
     */
    public void setBirthday(String birthday){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(BIRTHDAY,birthday);
        editor.apply();
    }
    public void setGender(String gender){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GENDER,gender);
        editor.apply();
    }
    public void setEmail(String email){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(EMAIL,email);
        editor.apply();
    }
    public void setPhoneNumber(String phoneNumber){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PHONE_NUMBER,phoneNumber);
        editor.apply();
    }
    public void setAccessToken(String accessToken){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_TOKEN,accessToken);
        editor.apply();
    }
    public void setRefreshToke(String refreshToke){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REFRESH_TOKE,refreshToke);
        editor.apply();
    }
    public void setCity(String city){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CITY,city);
        editor.apply();
    }
    public void setAvatarLink(String avatarLink){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(AVATAR_LINK,avatarLink);
        editor.apply();
    }

    public void setLoggedIn(boolean state){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOGGED_IN,state);
        editor.apply();
    }
    public void clearAllInfo(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }



}
